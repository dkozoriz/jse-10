package ru.t1.dkozoriz.tm.component;

import ru.t1.dkozoriz.tm.api.*;
import ru.t1.dkozoriz.tm.constant.ArgumentConst;
import ru.t1.dkozoriz.tm.constant.CommandConst;
import ru.t1.dkozoriz.tm.controller.CommandController;
import ru.t1.dkozoriz.tm.controller.ProjectController;
import ru.t1.dkozoriz.tm.controller.TaskController;
import ru.t1.dkozoriz.tm.repository.CommandRepository;
import ru.t1.dkozoriz.tm.repository.ProjectRepository;
import ru.t1.dkozoriz.tm.repository.TaskRepository;
import ru.t1.dkozoriz.tm.service.CommandService;
import ru.t1.dkozoriz.tm.service.ProjectService;
import ru.t1.dkozoriz.tm.service.TaskService;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);


    private void processCommands() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while (true) {
            System.out.println("ENTER COMMAND: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private void exit() {
        System.exit(0);
    }

    private void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit();
    }

    private void processCommand(final String command) {
        switch (command) {
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            case CommandConst.INFO:
                commandController.showSystemInfo();
                break;
            case CommandConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConst.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConst.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            default:
                commandController.showErrorCommand();
        }
    }

    private void processArgument(final String argument) {
        switch (argument) {
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            default:
                commandController.showErrorArgument();
        }
    }

    public void run(final String... args) {
        processArguments(args);
        processCommands();
    }

}