package ru.t1.dkozoriz.tm.constant;

import java.text.DecimalFormat;

public final class FormatConst {

    public static final DecimalFormat DF = new DecimalFormat("#.##");

    public static final long KILOBYTE = 1024;

    public static final double MEGABYTE = KILOBYTE * 1024;

    public static final double GIGABYTE = MEGABYTE * 1024;

    public static final double TERABYTE = GIGABYTE * 1024;

}