package ru.t1.dkozoriz.tm.util;

import ru.t1.dkozoriz.tm.constant.FormatConst;

public interface FormatUtil {

    static String formatBytes(final long bytes) {
        if ((bytes >= 0) && (bytes < FormatConst.KILOBYTE)) {
            return bytes + " B";
        } else if ((bytes >= FormatConst.KILOBYTE) && (bytes < FormatConst.MEGABYTE)) {
            return FormatConst.DF.format(bytes / FormatConst.KILOBYTE) + " KB";
        } else if ((bytes >= FormatConst.MEGABYTE) && (bytes < FormatConst.GIGABYTE)) {
            return FormatConst.DF.format(bytes / FormatConst.MEGABYTE) + " MB";
        } else if ((bytes >= FormatConst.GIGABYTE) && (bytes < FormatConst.TERABYTE)) {
            return FormatConst.DF.format(bytes / FormatConst.GIGABYTE) + " GB";
        } else if (bytes >= FormatConst.TERABYTE) {
            return FormatConst.DF.format(bytes / FormatConst.TERABYTE) + " TB";
        } else {
            return bytes + " Bytes";
        }
    }

}