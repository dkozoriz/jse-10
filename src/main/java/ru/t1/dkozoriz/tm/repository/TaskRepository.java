package ru.t1.dkozoriz.tm.repository;

import ru.t1.dkozoriz.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ru.t1.dkozoriz.tm.api.ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task add(final Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

}