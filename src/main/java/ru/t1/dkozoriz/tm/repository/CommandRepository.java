package ru.t1.dkozoriz.tm.repository;

import ru.t1.dkozoriz.tm.api.ICommandRepository;
import ru.t1.dkozoriz.tm.constant.ArgumentConst;
import ru.t1.dkozoriz.tm.constant.CommandConst;
import ru.t1.dkozoriz.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command VERSION = new Command(CommandConst.VERSION, ArgumentConst.VERSION, "show version info.");

    private static final Command ABOUT = new Command(CommandConst.ABOUT, ArgumentConst.ABOUT, "show developer info.");

    private static final Command HELP = new Command(CommandConst.HELP, ArgumentConst.HELP, "show command list.");

    private static final Command INFO = new Command(CommandConst.INFO, ArgumentConst.INFO, "close application.");

    private static final Command EXIT = new Command(CommandConst.EXIT, null, "show system info.");

    private static final Command PROJECT_LIST = new Command(CommandConst.PROJECT_LIST,null, "show project list.");

    private static final Command PROJECT_CREATE = new Command(CommandConst.PROJECT_CREATE,null, "create new project.");

    private static final Command PROJECT_CLEAR = new Command(CommandConst.PROJECT_CLEAR,null, "delete all projects.");

    private static final Command TASK_LIST = new Command(CommandConst.TASK_LIST,null, "show task list.");

    private static final Command TASK_CREATE = new Command(CommandConst.TASK_CREATE,null, "create new task.");

    private static final Command TASK_CLEAR = new Command(CommandConst.TASK_CLEAR,null, "delete all tasks.");

    private static final Command[] COMMANDS = new Command[] {
            VERSION, ABOUT, HELP, INFO, EXIT,
            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            TASK_LIST, TASK_CREATE, TASK_CLEAR
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}