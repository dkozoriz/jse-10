package ru.t1.dkozoriz.tm.api;

import ru.t1.dkozoriz.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}