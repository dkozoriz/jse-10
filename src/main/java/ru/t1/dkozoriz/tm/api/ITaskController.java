package ru.t1.dkozoriz.tm.api;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();

}