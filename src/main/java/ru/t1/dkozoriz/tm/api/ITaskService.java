package ru.t1.dkozoriz.tm.api;

import ru.t1.dkozoriz.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

}